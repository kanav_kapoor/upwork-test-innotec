<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginPageTest extends TestCase
{
    /**
     * Check correct view is rendered
     *
     * @return void
     */
    public function testAuthLoginViewIsRendered()
    {
        $response = $this->get('/login');
        
        $response->assertViewIs('auth.login');
    }

    /**
     * CHeck status code
     *
     * @return void
     */
    public function testStatusCode()
    {
        $response = $this->get('/login');
        
        $response->assertStatus(200);
    }

    /**
     * CHeck unsuccessful submission
     *
     * @return void
     */
    public function testIncorrectCredentialsSubmission()
    {
        $response = $this->post('/login', [
            'username' => 'kanav.kapoor',
            'password' => 'WrongPassword',
        ]);
        
        $response->assertSessionHasErrors(['username']);
    }

    /**
     * CHeck successful submission
     *
     * @return void
     */
    public function testCorrectCredentialsSubmission()
    {
        $response = $this->post('/login', [
            'username' => 'kanav.kapoor',
            'password' => 'Craft@2017',
        ]);
        
        $response->assertRedirect('/dashboard');
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testAlreadyAuthorisedRedirect()
    {
        $user = User::first();

        $this->be($user);

        $response = $this->get('/login');

        $response->assertStatus(302);
    }
}
