<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DashboardPageTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testUnauthorisedAccess()
    {
        $response = $this->get('/dashboard');

        $response->assertStatus(302);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testAuthorisedAccess()
    {
        $user = User::first();

        $this->be($user);

        $response = $this->get('/dashboard');

        $response->assertStatus(200);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testDashboardViewIsRendered()
    {
        $user = User::first();

        $this->be($user);

        $response = $this->get('/dashboard');
        
        $response->assertViewIs('dashboard.index');
        $response->assertViewHasAll(['keyword', 'sortBy', 'entries']);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testDashboardViewHasData()
    {
        $user = User::first();

        $this->be($user);

        $response = $this->get('/dashboard');

        $response->assertViewHasAll(['keyword', 'sortBy', 'entries']);
    }


}
