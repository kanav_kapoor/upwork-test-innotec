<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'     => 'Kanav Kapoor',
            'email'    => 'kanav.kapoor@canvascraft.media',
            'username' => 'kanav.kapoor',
            'password' => bcrypt('Craft@2017'),

        ]);
    }
}
