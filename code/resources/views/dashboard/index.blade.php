@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-9 align-middle">
                            <h1 class="text-primary">Dave Machado Public Dataset</h1>
                        </div>
                        <div class="col-md-3">
                            <form action="{{ route('dashboard') }}">
                                <div class="input-group">
                                    <input type="text" name="keyword" value="{{ $keyword }}" class="form-control keyword" placeholder="Enter Keyword" aria-label="Enter Keywords" aria-describedby="basic-addon2">
                                    <input type="hidden" name="sort_by" class="sort-by">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="submit">Search</button>
                                    </div>
                                </div>
                                <small class="text-center"><a href="{{ route('dashboard') }}">RESET FILTER</a></small>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    {{-- @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif --}}

                    <table class="table">
                        <thead>
                            <tr>
                                <th class="w-25"><a href="javascript:void(0)" data-type="Category" class="sort-type">Category <i class="fa fa-sort-alpha-asc" aria-hidden="true"></i></a></th>
                                <th class="w-25"><a href="javascript:void(0)" data-type="API" class="sort-type">Sub Category <i class="fa fa-sort-alpha-asc" aria-hidden="true"></i></a></th>
                                <th class="w-25"><a href="javascript:void(0)" data-type="Description" class="sort-type">Description <i class="fa fa-sort-alpha-asc" aria-hidden="true"></i></a></th>
                                <th class="w-25"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($entries as $entry)
                                <tr>
                                    <td>{{ $entry['Category'] }}</td>
                                    <td>{{ $entry['API'] }}</td>
                                    <td>{{ $entry['Description'] }}</td>
                                    <td><a href="{{ $entry['Link'] }}">Details</a></td>
                                </tr>
                            @endforeach
                        </tbody>

                        <tfoot>
                            <tr>
                                <td colspan="4" class="col-md-12">{{ $entries->appends(['keyword' => $keyword, 'sort_by' => $sortBy])->links() }}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script>
        $(function(){
            const $sortType = $('.sort-type'),
            $sortByInput = $('.sort-by');
            $keyword = $('.keyword');

            $sortType.on('click', submitForm);

            function submitForm(e) {
                const $this = $(this);
                
                $sortByInput.val($this.data('type'))
                $keyword.val($keyword.val());
                $sortByInput.closest('form').trigger('submit');
            }

        });
    </script>
@endpush