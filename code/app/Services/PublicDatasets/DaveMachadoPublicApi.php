<?php

namespace App\Services\PublicDatasets;

use Log;
use Exception;
use App\Utilities\ApiConnection;

class DaveMachadoPublicApi
{
    /**
     * Base URL of the API
     * @var string
     */
    const BASE_URL = 'https://api.publicapis.org';
    
    /**
     * Instance of Api Connection module
     * @var App\Utilities\ApiConnection
     */
    private $apiConnection;

    public function __construct()
    {
        $this->apiConnection = new ApiConnection(self::BASE_URL);
    }

    /**
     * Get entried from API
     * @param  string  $keyword
     * @param  string  $sortBy 
     * @param  boolean $paginated
     * @param  integer $pages
     * @return Illuminate\Http\Collection
     */
    public function entries($keyword = null, $sortBy = null, $paginated = true, $pages = 10)
    {
        try {
            $entriesDatasetCollection = collect( $this->apiConnection->make('entries')['entries'] );

            $entriesDatasetCollection = $this->filter($entriesDatasetCollection, $keyword, $sortBy, $paginated, $pages);

            return $entriesDatasetCollection;

        } catch (Exception $exception) {
            \Log::info($exception->getMessage());
            return false;
        }
    }

    /**
     * Filter collection based on different criteria
     * @param  \Illiminate\Support\Collection $entriesDatasetCollection
     * @param  string $keyword
     * @param  string $sortBy
     * @param  boolean $paginated
     * @param  int $pages
     * @return \Illiminate\Support\Collection
     */
    private function filter($entriesDatasetCollection, $keyword, $sortBy, $paginated, $pages)
    {
        if($keyword){
            $entriesDatasetCollection = $this->searchByKeyword($entriesDatasetCollection, $keyword);
        }

        if($sortBy){
            $entriesDatasetCollection = $entriesDatasetCollection->sortBy($sortBy);
        }

        if($paginated){
            $entriesDatasetCollection = $entriesDatasetCollection->paginate($pages);
        }

        return $entriesDatasetCollection;
    }

    /**
     * Filter dataset by keywords
     * @param  \Illuminate\Support\Collection $entriesDatasetCollection
     * @param  string $keyword
     * @return \Illuminate\Support\Collection
     */
    private function searchByKeyword($entriesDatasetCollection, $keyword)
    {
        return $entriesDatasetCollection->filter(function($entry, $key) use ($keyword){
                // !== is required as  string position of keywords
                // that are in the beginnning with return 0
                // which is considered as false
                return
                     stripos($entry['API'], $keyword) !== false
                    || stripos($entry['Description'], $keyword) !== false
                    || stripos($entry['Category'], $keyword) !== false;
        });
    }
}
