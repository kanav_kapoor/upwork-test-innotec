<?php

namespace App\Utilities;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class ApiConnection
{
    /**
     * Base URL of the API
     * @var string
     */
    protected $baseUrl;

    public function __construct($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * Make connection to API
     *
     * @param  string       $url          Url of the api
     * @param  string       $type         GET, POST, PUT, PATCH
     * @param  array        $parameters   form-data, parameters
     * @return Collection
     */
    public function make($url = null, $type = "get", $parameters = null)
    {
        $client = $this->getClientInstance();

        try {
            if ($type == 'get') {

                $response = $client->get($this->baseUrl . '/' . $url, [
                    'query'           => $parameters,
                    'allow_redirects' => [
                        'max' => 10,
                    ],
                ]);

            } elseif ($type == 'put') {
                $response = $client->put($this->baseUrl . '/' . $url, [
                    'form_params' => $parameters,
                ]);

            } elseif ($type == 'post') {

                $response = $client->post($this->baseUrl . '/' . $url, [
                    'form_params' => $parameters,
                ]);

            } elseif ($type == 'postWithFile') {

                $response = $client->post($this->baseUrl . '/' . $url, [
                    'multipart' => $parameters,
                ]);

            } elseif ($type == 'patch') {
                $response = $client->patch($this->baseUrl . '/' . $url);
            }

            $status = $response->getStatusCode();

            if ($status == 200) {
                return collect(json_decode($response->getBody(), true));
            } else {
                throw new \Exception('Failed');
            }

        } catch (Exception $exception) {
            \Log::info($exception->getMessage());
            return $exception->getMessage();
        }

    }

    /**
     * Sets header and instantiates Client
     *
     * @return Client  $object
     */
    protected function getClientInstance()
    {
        $headers = [
            'Content-Type'  => 'application/json'
        ];

        $object = new Client([
            'headers' => $headers,
        ]);

        return $object;
    }
}
