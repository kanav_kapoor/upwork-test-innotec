<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PublicDatasets\DaveMachadoPublicApi;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->keyword;
        $sortBy  = $request->sort_by;

        $entries = (new DaveMachadoPublicApi())
                        ->entries($keyword, $sortBy);

        // In case the url is down, Exception is throw and catched.
        // Relevant information is stored in laravel.log file
        // It's good to display 404 page in this case
        if(!$entries) abort(404);

        return view('dashboard.index', compact('keyword', 'sortBy', 'entries'));
    }
}
